# Managing sensitive data in Puppet

## Introduction

Puppet allows for a separation between code and data through the use of Hiera.
This creates a (very useful) distinction between module authors and module
users.

However, this separation is not sufficient for a secure setup. By default, when
using the YAML backend, all Hiera data is plaintext. So, all sensitive data
contained in Hiera is readable to everyone who has access to the Hiera data.
Moreover, when the Hiera data is version controlled, and a hosting provider is
used (eg. GitHub, BitBucket, ...), all secrets are available remotely in
plaintext.

Of course, versioning the Hiera data comes with a lot of benefits. So the
problem is really the access to the sensitive data.

A number of different solutions address this, among which:

* separating the data from the sensitive data, with tighter access control on
  the latter.
* [Hiera-gpg](https://github.com/crayfishx/hiera-gpg), which encrypts the entire
  data file, thus obfuscating all sensitive information contained therein. This
  can be combined with the first solution, to have only the data containing
  sensitive information encrypted.
* [Hiera-eyaml](https://github.com/TomPoulton/hiera-eyaml), which is capable of
  encrypting specific keys in a data file. This allows for a somewhat more
  flexible approach, as all related data is kept together, with only the
  security sensitive parts encrypted.

As the third option seems to receive the most praise, due to the flexibility
and fine-grained control, this one was explored further. Moreover, it provides
some tooling to make the decryption/encryption as effortless as possible.

## Hiera-eyaml

[Hiera-eyaml](https://github.com/TomPoulton/hiera-eyaml) is a pluggable backend
for your hiera data that can contain secrets encrypted through different
mechanisms. By default hiera-eyaml uses a PKCS7 public/private keypair to
protect the secrets, but a GPG backend it also available in
[hiera-eyaml-gpg](https://github.com/sihil/hiera-eyaml-gpg), allowing secrets
to be protected using asymmetric encryption and the use of GPG keys.

The puppetmaster of course will need access to the secrets in order to provide
them to puppet nodes (whether using PKCS7 or GPG keys), so hiera-eyaml does
nothing to help secure the master itself. You should work on the basis that the
secrets are effectively plaintext on the puppetmaster and protect it
appropriately. If someone does compromise your puppetmaster, you have bigger
problems than someone being able to read the hiera secrets. However,
hiera-eyaml does protect the secrets while they reside outside of the
puppetmaster, for example on workstations and in version control systems.

## Setup

### Installation

As hiera-eyaml is a ruby project, it is packaged as a gem. This is fine for
development, but somewhat harder for deployment on a production environment. To
make deployment easier, a number of deb packages was created from the gems.
The following packages are needed:

* ruby-gpgme (available in the Ubuntu universe repository)
* rubygem-trollop
* rubygem-highline (< 1.7.0)
* rubygem-hiera-eyaml
* rubygem-hiera-eyaml-gpg

The usual installation instructions (tested on Ubuntu 14.04 LTS) apply here:

```shell
$ sudo apt-get install ruby-gpgme
$ sudo dpkg -i rubygem-trollop_2.1.2_all.deb
$ sudo dpkg -i rubygem-highline_1.6.21_all.deb
$ sudo dpkg -i rubygem-hiera-eyaml_2.0.8_all.deb
$ sudo dpkg -i rubygem-hiera-eyaml-gpg_0.5.0_all.deb
```

### Creating keys

With the software installed, we can start creating the necessary keys for
encryption/decryption.

#### PKCS7

The PKCS7 backend is the default backend for hiera-yaml. Configuration happens
through a YAML based config file, defaulting to **~/.eyaml/config.yaml**.
Create this file before generating the keys, with the following content:

```yaml
---
pkcs7_private_key: '~/.eyaml/keys/private_key.pkcs7.pem'
pkcs7_public_key: '~/.eyaml/keys/public_key.pkcs7.pem'
```

Keys can now be created with the following commands:

```shell
$ mkdir ~/.eyaml && cd ~/.eyaml
$ eyaml createkeys
```

Since the point of using this module is to securely store sensitive
information, it's important to store these keys securely. If using Hiera with
Puppet, your puppetmaster will need to access these keys to perform decryption
when the puppet agent runs on a remote node. So for this reason, a suggested
location might be to store them in **/var/lib/puppet/secure/keys**.

The permissions for this folder should allow the puppet user (normally
'puppet') execute access to the keys directory, read only access to the keys
themselves and restrict everyone else:

```shell
$ sudo chown -R puppet:puppet /var/lib/puppet/secure/keys
$ sudo chmod -R 0500 /var/lib/puppet/secure/keys
$ sudo chmod 0400 /var/lib/puppet/secure/keys/*.pem
$ sudo ls -lha /var/lib/puppet/secure/keys
-r-------- 1 puppet puppet 1.7K Sep 24 16:24 private_key.pkcs7.pem
-r-------- 1 puppet puppet 1.1K Sep 24 16:24 public_key.pkcs7.pem
```

Create a configuration file **/var/lib/puppet/.eyaml/config.yaml** on the
puppetmaster (assuming /var/lib/puppet is the home directory of the puppet
user), that reflects the location of the keys:

```yaml
---
pkcs7_private_key: '/var/lib/puppet/secure/keys/private_key.pkcs7.pem'
pkcs7_public_key: '/var/lib/puppet/secure/keys/public_key.pkcs7.pem'
```

#### GPG

The optional GPG backend (contained in the gem hiera-eyaml-gpg) allows for more
flexibility and granularity, at the cost of a somewhat more complex setup
procedure.

First, we need to create the puppetmaster keys on the puppetmaster:
```shell
$ sudo -u puppet mkdir /var/lib/puppet/.gnupg
$ sudo -u puppet gpg --homedir /var/lib/puppet/.gnupg --gen-key
```

Now export the public part of the key (be sure not to set a passphrase here):
```shell
$ sudo -u puppet gpg --homedir /var/lib/puppet/.gnupg --export -a
```

Copy the public key to your local workstation using something like scp so you
can encrypt data using it later.

If you don’t already have a personal GPG keypair, create one for yourself now:
```shell
$ gpg --gen-key
```

Next, you need to import all of the puppet master keys and collected keys from
your coworkers into your keyring.
```shell
gpg --import puppetmaster.pub
gpg --import user1.pub
```

Finally, sign the imported keys with your personal key:
```shell
gpg --sign-key <key id/email of puppetmaster key>
gpg --sign-key <key id/email of coworker>
```

Point hiera-eyaml to the keys with the following config file (in
**~/.eyaml/config.yaml**) on your workstation:
```yaml
---
encrypt_method: 'gpg'
gpg_gnupghome: '~/.gnupg'
gpg_recipients: 'user1@example.com,user2@example.com,puppetmaster@example.com'
```

Do the same on the puppetmaster. The configuration is stored in
**/var/lib/puppet/.eyaml/config.yaml**:
```yaml
---
encrypt_method: 'gpg'
gpg_gnupghome: '/var/lib/puppet/.gnupg'
gpg_recipients: 'user1@example.com,user2@example.com,puppetmaster@example.com'
```

### Initial tests

You can now test the functionality of the encryption/decryption. Use the
**eyaml** binary for this:
```shell
$ eyaml encrypt -s 'secret string'
```

This will result in a long encoded string wrapped in **ENC[GPG, ]** or
**ENC[PKCS7, ]**, depending on the encryption mechanism you configured. This
string can be decrypted on all machines that either have the correct PKCS7
keys, or were added to the gpg_recipients and had their GPG key signed by the
encrypting party. You can test this by issuing:
```shell
$ eyaml decrypt -s 'ENC[GPG,...]'
```

On the puppetmaster, this can be tested using the following command:
```shell
$ sudo -u puppet eyaml decrypt --gpg-gnupghome=/var/lib/puppet/.gnupg -n gpg -s 'ENC[GPG,...]'
```

This should return the original decrypted string.

### Hiera setup

With the encryption/decryption working, the hiera integration is all that is
left. Please make sure that the files containing the secrets are at the top of
the hierarchy, above the machine level. This prevents accidental overrides of
credentials.

Also, don't forget to restart the puppetmaster after making changes to the
hiera config.

#### PKCS7

For PKCS7, add the following to **/etc/puppet/hiera.yaml**:
```yaml
---
:backends:
    - eyaml
    - yaml

:hierarchy:
    - secure
    - %{fqdn}
    - common

:yaml:
    :datadir: '/etc/puppet/hieradata'

:eyaml:
    :extension: 'yaml'
    :datadir: '/etc/puppet/hieradata'
    :pkcs7_private_key: /var/lib/puppet/.eyaml/keys/private_key.pkcs7.pem
    :pkcs7_public_key:  /var/lib/puppet/.eyaml/keys/public_key.pkcs7.pem
```

#### GPG

For GPG, add the following to **/etc/puppet/hiera.yaml**:
```yaml
---
:backends:
    - eyaml
    - yaml

:hierarchy:
    - secure
    - %{fqdn}
    - common

:yaml:
    :datadir: '/etc/puppet/hieradata'

:eyaml:
    :extension: 'yaml'
    :datadir: '/etc/puppet/hieradata'
    :gpg_gnupghome: '/var/lib/puppet/.gnupg'
```

## Usage

### Adding or editing secrets

Now we’re all set, it’s time for the fun bit. We’ll use the eyaml tool to edit
the secure.yaml to add a new secret value.
```shell
$ eyaml edit secure.yaml
```

Add a new value into the file, wrapping the plaintext secret in **DEC::GPG[**
and **]!**. This will tell eyaml that the secret is in decrypted form, and
should be encrypted with the GPG backend.

```yaml
---
host::root_password: DEC::GPG[super secret]!
```

Now save and exit the file, then re-open it with a text editor, and you’ll see
something like this instead:

```yaml
---
host::root_password: ENC[GPG,hQEMA2HEQlKJqfdZAQgAkt/gddRjel3P6b9t0ny5Xk9F5IO9cyhGSahp0lUw5Wn9k9kkZsAbWMi4w9fbZ0ZWh6PkXmyfhsOu6XndlEb6EMrrrUB0W5mcfggPUHsUZnbbyXb6wNFS4I+Tgwjwwt/FmVi5/KrEA6TZSRYbH1+eWryRYfa+U8xmf89XjLOIuhqkwEkwtFNFFiBAeXTfxuBVN6iFJkLXhKWOT7DpM1NGz+ppcFkchBHDwOYqqWrDZ0PM41L+dLgIcziY78jthqmmPuwtw9WplLvQC2uD93vaib7PJuRKtuugB34Jwn3uR+5+nhMwUMD2dsXGdj71o9x7ehHH4FRwQ4v5RsGYNVaFe4UBDAMUAqkCE6CtbgEH/1TNC9cCl+yfxA0goZn6QMd/VScObYT1AHcKWD630nzzo9pgKckcDC3VB4LlQ+6iO/MNqOkY/YULr853QcZFmbRoLMo8x77J6lncOViams8L2LuHtoC0tnzshJzehCx9sNEpuZyFILI8pNlxdkhuhtgQUM8ifBoninaRAiA4rbrv87KwON9AePxm3KwaCGQmc43wgRLyBq6MgThkz4tsOH0oILaV54Q6WMX8WxYL5772wbkWl2Up67BpI16MwvxcScu1A5Hhk7vVgagd++CNx8gSbTI48vIG+PVvwwuHJiHZATUe+Q3EQRKRhC0pr/XRH4A81O3JyLqZUOIBRF1/gr7SSgEg5EMrG+r1pSN+wp4Qti9HkLZn/zwLbvqTDlQusaIqKf8FKspB9ttF09V6rKs/OhZNqKMpNVrNoBFeCI5LkhXb6SrpY5gJ5Yrt]
```

The password is encrypted. Success!

Running eyaml edit again will decrypt the file and open it in your text editor.
Saving and quitting the editor will re-encrypt the file again. While editing
the file, the wrapper string will change to **DEC(N)::GPG** where N is a unique
number within the file. This is used by eyaml to keep track of which values
have been edited so you don’t see all the encrypted blobs change every time the
file is edited, and only the actually changed values will show up in version
control diffs.

If you change a previously encrypted value, you have to remove the unique
number from the string wrapper. So make sure an edited key-value pair looks
like:

```yaml
host::root_password: DEC::GPG[new super secret]!
```

### Adding or removing users

If, in the future, you need to deploy a new puppetmaster or a new user joins
the team there are three things you need to do:

* Import the new user’s key into your keyring and sign it:

```shell
$ gpg --import newuser.pub
$ gpg --sign <keyid>
```

* Update the list of key recipients (in ~/.eyaml/config.yaml) with the name of the new keys:

```yaml
---
encrypt_method: 'gpg'
gpg_gnupghome: '~/.gnupg'
gpg_recipients: 'newuser1@example.com,puppetmaster@example.com'
```

* Re-encrypt the files that contain secrets so they are encrypted with the new set of keys:

```shell
$ eyaml recrypt <filename>
```

## Caveats

### Preventing information leaks

Secrets are now protected on disk and in version control, which is good, but
that’s not the only place secrets can be leaked. When you do a **puppet
--noop** run to see what things puppet would change, puppet will show diffs of
files, secrets and all. This will also be logged to syslog by default. And if
you’re running with stored configs/puppetdb the reports including the diffs
will be stored there as well.

You can bypass reporting by setting the **show_diff** parameter to **false** on
sensitive file resources. This has the slight downside that doing a --noop run
no longer shows you what would change in the file, but that’s probably better
than having a password available in plaintext somewhere.

## References

* [https://blog.benroberts.net/2014/12/setting-up-hiera-eyaml-gpg/](https://blog.benroberts.net/2014/12/setting-up-hiera-eyaml-gpg/)
* [https://github.com/TomPoulton/hiera-eyaml](https://github.com/TomPoulton/hiera-eyaml)
* [https://github.com/sihil/hiera-eyaml-gpg](https://github.com/sihil/hiera-eyaml-gpg)
* [https://puppetlabs.com/blog/encrypt-your-data-using-hiera-eyaml](https://puppetlabs.com/blog/encrypt-your-data-using-hiera-eyaml)
* [http://www.theguardian.com/info/developer-blog/2014/feb/14/encrypting-sensitive-data-in-puppet](http://www.theguardian.com/info/developer-blog/2014/feb/14/encrypting-sensitive-data-in-puppet)
