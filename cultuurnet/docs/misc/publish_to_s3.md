# Publishing static websites to Amazon S3

This document describes an automated workflow for uploading static content to an Amazon S3 bucket,
and publishing it as a website. The process is triggered by a source code commit.

## Overview

The picture below explains the workflow:

![Publish workflow](images/s3_publish.png)

In short, the sequence goes like:

1. Developer commits code for static website on his/her PC.
2. Developer pushes code upstream to Bitbucket repository.
3. Bitbucket hook on repository triggers Jenkins job.
4. Jenkins job verifies commit and prepares everything for publishing.
5. Jenkins job publishes the new content to Amazon S3.

Afterwards, the newly committed code is publicly available.

## Step description

### Developer commits code locally

The code for the static sites is hosted on Bitbucket, in the repository **cultuurnet/prototypes.git**.
You need to clone this repository to your machine before you can start adding code:

```shell
$ git clone git@bitbucket.org:cultuurnet/prototypes.git
```

The repository contains the static websites you wish to create as top-level directories.
Adding a new site is as simple as adding a new top-level directory and putting the appropriate files
in it. After that, you are ready to commit the new code. Just use the regular git commands to do so.

```shell
$ git add .
$ git commit -m "New static website"
```

### Developer pushes code upstream to Bitbucket

After the local commit, you will need to push your changes upstream. Make sure you push to the
remote master branch, as only this branch will be published. In case the **cultuurnet/prototypes.git**
repository is the origin (will be so if you followed the steps above), just issue:

```shell
$ git push origin master
```

### Bitbucket hook triggers Jenkins job

The **cultuurnet/prototypes.git** repository on Bitbucket has been configured with a *post-receive hook*.
This hook issues a POST request to a configured URL, in this case to the Jenkins server.

```
POST https://jenkins.uitdatabank.be/bitbucket-hook/
```

Because the Bitbucket plugin is installed in Jenkins, reception of the POST request will trigger a search
across all Jenkins jobs for one that contains the repository the hook was configured on.
In our case, the job called [*Publish_static_HTML_to_Amazon_S3*](https://jenkins.uitdatabank.be/job/Publish_static_HTML_to_Amazon_S3/)
will be triggered.

### Jenkins job verifies and prepares publishing

The Jenkins job performs a number of actions:

When no appropriate bucket is present:

1. Create new Amazon S3 bucket with name matching the site added or edited in the last commit.
2. Apply appropriate bucket policy to allow public read access to the new bucket and all necessary write operations by Jenkins.
3. Add bucket tag to make sure the bucket is easily verifiable as being created by the job.
   `created-by: https://jenkins.uitdatabank.be/job/Publish_static_HTML_to_Amazon_S3/`
4. Create static website from the bucket. From this point on, the content of the bucket is globally accessible on the URL
   `<website>.s3-website-eu-west-1.amazonaws.com`

When a bucket with the correct name is already present, the following actions are performed:

1. Remove content from existing Amazon S3 bucket whose name matches the site added or edited in the last commit, if this
   bucket contains the following tag:
   `created-by: https://jenkins.uitdatabank.be/jobs/Publish_static_HTML_to_Amazon_S3/`

All Amazon interactions are performed by the [AWS CLI](http://aws.amazon.com/cli/) and [s3cmd](http://s3tools.org/s3cmd),
configured on the Jenkins server.

### Jenkins job publishes new content.

When all preceding actions succeed, the last step is the actual uploading of the new content to the bucket. This is done
in a post-build action of the Jenkins job, that recursively publishes all content in the added/edited directory from the
last commit to the bucket.

## Limitations

Due to the nature of Amazon S3 being used for publishing, and a few shortcuts taken in the implementation of the job, a
number of limitations apply:

* **one changed top-level directory per push**: changes pushed to Bitbucket should be limited to one top-level directory. If multiple
  websites need to be updated, multiple commit/push sequences will be needed.
* **index.html as entry point**: the Amazon S3 buckets are configured to serve web content with an index.html as entry point.
* **bucket naming rules**: Amazon S3 buckets are globally used, and therefore have to be globally unique. The names should be between 3
  and 63 characters, all lowercase. All rules are available [here](http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html).
* **jenkins job name without spaces**: Amazon S3 imposes some limitations on the values of bucket tags. As we use the Jenkins
  job URL as tag value, it should comply to these rules. One of the most strict is the lack of percent sign. As a space character
  in a job name is converted to '%20', this violates the naming rules.
