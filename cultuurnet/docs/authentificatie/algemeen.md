# Authentificatie

CultuurNet gebruikt de open standaard oAuth voor authenticatie.  Voor bijna alle programmeertalen zijn er libraries beschikbaar, meer info op http://oauth.net/code.

Er zijn 2 manieren om toegang te verkrijgen tot onze API's:

-	UserAccessToken: een UserAccessToken geeft de Service Consumer toegangsrechten tot de UiTiD API namens de gebruiker

- Consumer Access: via een Consumer Request kan de Service Consumer bepaalde acties uitvoeren die typisch niet gelinkt zijn aan 1 gebruiker, zoals bv. een zoekopdracht.

Afhankelijk van het soort request moet al dan niet een UserAccessToken gebruikt worden. Dit staat steeds in de documentatie beschreven.
 

 
